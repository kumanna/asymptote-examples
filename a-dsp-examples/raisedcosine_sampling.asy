import graph;
settings.outformat="svg";
size(8cm,4cm,IgnoreAspect);

defaultpen(fontsize(8pt));

typedef real realfcn(real);

realfcn rrc(real beta) {
  return new real(real f) {
    if (abs(f) < (1 - beta) / 2) return 1;
    else if (abs(f) > (1 - beta) / 2 && abs(f) < (1 + beta) / 2) {
      return 0.5 * (1 + cos(pi / beta * (abs(f) - (1 - beta) / 2)));
    }
    else { return 0; }
  };
}

xaxis("$f$", -2.0, 2.0, Arrow,above=true);
yaxis("$H(f)$",0, 1.5, above=true,arrow=Arrow,autorotate=false);
xtick("$-\frac{1}{2T}$", (-0.5,-0.08),dir=N);
xtick("$\frac{1}{2T}$", (0.5,-0.08));
xtick("$-\frac{3}{2T}$", (-1.5,-0.08),dir=N);
xtick("$\frac{3}{2T}$", (1.5,-0.08));
xtick("$0$", (0.0,0.0),dir=NW,size=0);
draw(graph(rrc(0.5),-1, 1));
shipout(prefix="rrc_sampling_1");
draw(shift(1,0)*graph(rrc(0.5),-1, 1),red+dotted);
draw(shift(-1,0)*graph(rrc(0.5),-1, 1),red+dotted);
label("$H(f - 1 / T)$", (1,1),N,red);
label("$H(f + 1 / T)$", (-1,1),N,red);
shipout(prefix="rrc_sampling_2");
draw((-1.25,1)--(1.25,1));
label(scale(0.6)*Label("$\displaystyle{\sum_{m = -\infty}^\infty H(f + m / T) = 1}$"), (1,1.35),black,Draw);
shipout(prefix="rrc_sampling_3");
