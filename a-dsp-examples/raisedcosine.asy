import graph;
settings.outformat="svg";
size(8cm,4cm,IgnoreAspect);

defaultpen(fontsize(8pt));

typedef real realfcn(real);

realfcn rrc(real beta) {
  return new real(real f) {
    if (abs(f) < (1 - beta) / 2) return 1;
    else if (abs(f) > (1 - beta) / 2 && abs(f) < (1 + beta) / 2) {
      return 0.5 * (1 + cos(pi / beta * (abs(f) - (1 - beta) / 2)));
    }
    else { return 0; }
  };
}

yaxis("$H(f)$",0, 1.5, above=true,arrow=Arrow,autorotate=false);
xtick("$-\frac{1}{2T}$", (-0.5,-0.08),dir=N);
xtick("$\frac{1}{2T}$", (0.5,-0.08));
xtick("$0$", (0.0,0.0),dir=NW,size=0);
draw((-0.5,0)--(-0.5,1)--(0.5,1)--(0.5,0), "$\beta = 0$");
draw(graph(rrc(0.25),-1, 1), red, "$\beta = 0.25$");
draw(graph(rrc(0.5),-1, 1), green, "$\beta = 0.5$");
draw(graph(rrc(0.75),-1, 1), blue, "$\beta = 0.75$");
xaxis("$f$", -1.0, 1.0, Arrow,above=true);
add(legend(linelength=10,ymargin=1,xmargin=1),point(E),20E,UnFill);
